module load art/mountrainier20160605
cd /home/projects/cge/people/pmun/projects/MGBinner/simulateData

#Sample 1
#Simulate reads from the references to different coverages
art_illumina -ss HS25 -i references/AL513382.1.fasta -p -l 150 -f 15 -m 300 -s 10 -o sample1/AL513382.1_
art_illumina -ss HS25 -i references/AP014597.1.fasta -p -l 150 -f 12 -m 300 -s 10 -o sample1/AP014597.1_
art_illumina -ss HS25 -i references/HF558398.1.fasta -p -l 150 -f 10 -m 300 -s 10 -o sample1/HF558398.1_
art_illumina -ss HS25 -i references/NC_002951.2.fasta -p -l 150 -f 8 -m 300 -s 10 -o sample1/NC_002951.2_
art_illumina -ss HS25 -i references/NC_016845.1.fasta -p -l 150 -f 6 -m 300 -s 10 -o sample1/NC_016845.1_
art_illumina -ss HS25 -i references/NZ_CCDV01000001.1.fasta -p -l 150 -f 5 -m 300 -s 10 -o sample1/NZ_CCDV01000001.1_
art_illumina -ss HS25 -i references/NZ_FBYD01000004.1.fasta -p -l 150 -f 4 -m 300 -s 10 -o sample1/NZ_FBYD01000004.1_

#Concatenate reads from different organisms
cat sample1/*_1.fq > sample1/sample1_1.fq
cat sample1/*_2.fq > sample1/sample1_2.fq

#Sample 2
#Simulate reads from the references to different coverages
art_illumina -ss HS25 -i references/AL513382.1.fasta -p -l 150 -f 4 -m 300 -s 10 -o sample2/AL513382.1_
art_illumina -ss HS25 -i references/AP014597.1.fasta -p -l 150 -f 5 -m 300 -s 10 -o sample2/AP014597.1_
art_illumina -ss HS25 -i references/HF558398.1.fasta -p -l 150 -f 6 -m 300 -s 10 -o sample2/HF558398.1_
art_illumina -ss HS25 -i references/NC_002951.2.fasta -p -l 150 -f 12 -m 300 -s 10 -o sample2/NC_002951.2_
art_illumina -ss HS25 -i references/NC_016845.1.fasta -p -l 150 -f 12 -m 300 -s 10 -o sample2/NC_016845.1_
art_illumina -ss HS25 -i references/NZ_CCDV01000001.1.fasta -p -l 150 -f 10 -m 300 -s 10 -o sample2/NZ_CCDV01000001.1_
art_illumina -ss HS25 -i references/NZ_FBYD01000004.1.fasta -p -l 150 -f 15 -m 300 -s 10 -o sample2/NZ_FBYD01000004.1_

#Concatenate reads from different organisms
cat sample2/*_1.fq > sample2/sample2_1.fq
cat sample2/*_2.fq > sample2/sample2_2.fq

#Sample 3
#Simulate reads from the references to different coverages
art_illumina -ss HS25 -i references/AL513382.1.fasta -p -l 150 -f 20 -m 300 -s 10 -o sample3/AL513382.1_
art_illumina -ss HS25 -i references/AP014597.1.fasta -p -l 150 -f 15 -m 300 -s 10 -o sample3/AP014597.1_
art_illumina -ss HS25 -i references/HF558398.1.fasta -p -l 150 -f 4 -m 300 -s 10 -o sample3/HF558398.1_
art_illumina -ss HS25 -i references/NC_002951.2.fasta -p -l 150 -f 7 -m 300 -s 10 -o sample3/NC_002951.2_
art_illumina -ss HS25 -i references/NC_016845.1.fasta -p -l 150 -f 9 -m 300 -s 10 -o sample3/NC_016845.1_
art_illumina -ss HS25 -i references/NZ_CCDV01000001.1.fasta -p -l 4 -f 10 -m 300 -s 10 -o sample3/NZ_CCDV01000001.1_
art_illumina -ss HS25 -i references/NZ_FBYD01000004.1.fasta -p -l 7 -f 15 -m 300 -s 10 -o sample3/NZ_FBYD01000004.1_

#Concatenate reads from different organisms
cat sample3/*_1.fq > sample3/sample3_1.fq
cat sample3/*_2.fq > sample3/sample3_2.fq

#Remove alignment files as they are not needed
rm sample*/*.aln

#Make interleaved fasta files from the PE fastq files
reformat.sh in=sample1/sample1_#.fq out=sample1/sample1reads.fa
reformat.sh in=sample2/sample2_#.fq out=sample2/sample2reads.fa
reformat.sh in=sample3/sample3_#.fq out=sample3/sample3reads.fa

#Make metagenomic assemblies on each of the three simulated microbiotas
/home/people/pmun/bin/idba_ud_151readlen --pre_correction --min_contig 500 --num_threads 28 --read sample1/sample1reads.fa --out sample1/assembly
/home/people/pmun/bin/idba_ud_151readlen --pre_correction --min_contig 500 --num_threads 28 --read sample2/sample2reads.fa --out sample2/assembly
/home/people/pmun/bin/idba_ud_151readlen --pre_correction --min_contig 500 --num_threads 28 --read sample3/sample3reads.fa --out sample3/assembly

#Make indexes for the metagenomic assemblies
cd sample1/readmap/ ; bbmap.sh reference=../assembly/scaffold.fa ; cd ../../
cd sample2/readmap/ ; bbmap.sh reference=../assembly/scaffold.fa ; cd ../../
cd sample3/readmap/ ; bbmap.sh reference=../assembly/scaffold.fa ; cd ../../

#Map reads 
cd sample1/readmap/
bbmap.sh in=../../sample1/sample1_#.fq nodisk covstats=sample1_covstats.txt covhist=sample1_covhist.txt
bbmap.sh in=../../sample2/sample2_#.fq nodisk covstats=sample2_covstats.txt covhist=sample2_covhist.txt
bbmap.sh in=../../sample3/sample3_#.fq nodisk covstats=sample3_covstats.txt covhist=sample3_covhist.txt
cd ../../

cd sample2/readmap/
bbmap.sh in=../../sample1/sample1_#.fq nodisk covstats=sample1_covstats.txt covhist=sample1_covhist.txt
bbmap.sh in=../../sample2/sample2_#.fq nodisk covstats=sample2_covstats.txt covhist=sample2_covhist.txt
bbmap.sh in=../../sample3/sample3_#.fq nodisk covstats=sample3_covstats.txt covhist=sample3_covhist.txt
cd ../../

cd sample3/readmap/
bbmap.sh in=../../sample1/sample1_#.fq nodisk covstats=sample1_covstats.txt covhist=sample1_covhist.txt
bbmap.sh in=../../sample2/sample2_#.fq nodisk covstats=sample2_covstats.txt covhist=sample2_covhist.txt
bbmap.sh in=../../sample3/sample3_#.fq nodisk covstats=sample3_covstats.txt covhist=sample3_covhist.txt
cd ../../

#Make a combined coverage / variance table for each of the crossmapped asssemblies
#See R code for how this was done

#Run metabat for each assembly
cd sample1
metabat -i assembly/scaffold.fa -o metaBin -a readmap/cov_var_table.txt --numThreads 0 --saveCls --unbinned -v -B 20 --pB 10 -s 100000
mv metaBin* binning/
cd ..

cd sample2
metabat -i assembly/scaffold.fa -o metaBin -a readmap/cov_var_table.txt --numThreads 0 --saveCls --unbinned -v -B 20 --pB 10 -s 100000
mv metaBin* binning/
cd ..

cd sample3
metabat -i assembly/scaffold.fa -o metaBin -a readmap/cov_var_table.txt --numThreads 0 --saveCls --unbinned -v -B 20 --pB 10 -s 100000
mv metaBin* binning/
cd ..
