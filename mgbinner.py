#! /usr/bin/python3

from __future__ import print_function
import argparse
import os
import os.path
import re
import subprocess
import sys

from submodules.seqfilehandler.SeqFileHandler import SeqFile
from submodules.dependencies.Dependencies import Dependencies
from submodules.moabtorque.MoabTorqueSubmission import JobCluster
from data_parse.joinCovtables import CoverageTable


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


#
# Handling arguments
#

# TODO: Write description
parser = argparse.ArgumentParser(description="TODO: DESCRIPTION MISSING")

# Input / Output options
parser.add_argument("-r", "--read_files",
                    help="Raw read data in FASTQ format.",
                    nargs='+',
                    metavar='FASTQ')
parser.add_argument("-a", "--assembly_files",
                    help="Metagenome assemblies",
                    nargs='+',
                    metavar='FASTA')
parser.add_argument("-o", "--outdir",
                    help="Path to directory in which the results will be\
                          stored.",
                    default=None,
                    metavar='OUT_DIR')

# TODO: Pipeline parameters

# Pipeline options
parser.add_argument("--checkm",
                    help="Run CheckM. NOTE: Not yet implemented!",
                    action="store_true",
                    default=False)

# Configuration and queue options
parser.add_argument("-t", "--tmp_dir",
                    help="Temporary directory for storage of the results\
                          from the external software and the queing system.",
                    default="mgbinner_tmp_dir")
parser.add_argument("-cp", "--config_prg",
                    help="Path to configuration file for external software.\
                          Default path is:\
                          'config/dependencies.txt' relative to the script\
                          directory. The configuration file\
                          expects a prg name and a path on each line of\
                          the file. Ex.: blat<tab>/usr/bin/blat. If a prg\
                          is found in your env. path. you can simply\
                          specify: prg<tab>prg_alias. Ex.: blat<tab>blat.",
                    metavar="CONF_PRG")
parser.add_argument("--hpc_size",
                    help="This option will artificially set the size of the\
                          hpc. If set to zero, the real hpc size will be used.\
                          Default is 2000, because this is the maximum no. of\
                          procs allowed to be used for each group on\
                          computerome.",
                    type=int,
                    default=2000)
parser.add_argument("-cq", "--config_queue",
                    help="Path to configuration file for queue.\
                          Default path is:\
                          'config/queue_conf.txt' relative to the script\
                          directory. The queue configuration file\
                          determines how to utilize the queue with regard to\
                          load balancing.",
                    metavar="CONF_QUEUE")
parser.add_argument("-q", "--queue",
                    help="Use a queing system for a computer cluster.",
                    metavar='QUEUE',
                    choices=["Computerome"],
                    default=None)

args = parser.parse_args()

# Set configuration file paths
if(not args.config_prg):
    args.config_prg = os.path.dirname(
        os.path.realpath(__file__)) + "/config/dependencies.txt"
    args.config_prg = os.path.abspath(args.config_prg)
if(not args.config_queue):
    args.config_queue = os.path.dirname(
        os.path.realpath(__file__)) + "/config/queue_conf.txt"
    args.config_queue = os.path.abspath(args.config_queue)

# Check if configuration files are found at the paths created.
if(not os.path.isfile(args.config_prg)):
    print("Configuration file not found:", args.config_prg)
    quit(1)
if(not os.path.isfile(args.config_queue)):
    print("Queue configuration file not found:", args.config_queue)
    quit(1)

# Load dependencies
prgs = Dependencies(args.config_prg, ["bbmap", "metabat"])

# Set internal script path
joinCovtables_path = os.path.dirname(
    os.path.realpath(__file__)) + "/data_parse/joinCovtables.py"
joinCovtables_path = os.path.abspath(joinCovtables_path)

# Adding script to prgs
prgs["joinCovtables"] = joinCovtables_path

# Create tmp dir unless it already exists
args.tmp_dir = os.path.abspath(args.tmp_dir)
os.makedirs(args.tmp_dir, exist_ok=True)

# Load files and pair them if necessary
read_files = SeqFile.parse_files(args.read_files)

# Save a list of sample names used for file naming.
# TODO: I think I have a method lying around somewhere to replace fa/fasta etc.
sample_names = []
for assembly in args.assembly_files:
    sample_name = assembly.replace(".fa", "")
    sample_names.append(sample_name)

# Creating output directories
for sample in sample_names:
    os.makedirs(args.outdir + "/" + sample, exist_ok=True)

# bbmap indexing commands
index_cmds = {}
for i, sample in enumerate(sample_names):
    cmd = ("cd " + args.outdir + "/" + sample + "\n"
           + prgs["bbmap"] + " reference=" + args.assembly_files[i] + "\n")
    index_cmds[sample] = cmd

# Mapping commands - map all reads to all scaffolds
# Again, I'm not sure how I specify a specific reference index. From BBs
# example, it seems like it just uses the 'ref' subdir in the working dir
map_cmds = {}
for sample in sample_names:
    map_cmds[sample] = {}
    for read_file in read_files:
        cmd = ("cd " + args.outdir + "/" + sample + "\n"
               + prgs["bbmap"]
               + " in=" + read_file.path
               + " in2=" + read_file.pe_file_reverse
               + " covstat={:}_covstats.txt".format(read_file.filename)
               + " covhist={:}_covhist.txt".format(read_file.filename) + "\n")
        map_cmds[sample][read_file.filename] = cmd

# TODO: Insert corrects paths
covstat_files = ["testData/sample1_covstats.txt",
                 "testData/sample2_covstats.txt",
                 "testData/sample3_covstats.txt"]
# Create a final coverage table with scaffold rows and name, length, avg_cov,
# sample1cov, sample1var, sampleNcov, sampleNvar
metabat_tab = CoverageTable.metaBatCoVarTable(covstat_files)

# Export the metabat-ready coverage table
# TODO: Insert correct output path
metabat_infile = "metabat_coVar.txt"
metabat_tab.to_csv(metabat_infile, sep="\t", float_format='%11.5f',
                   index=False)

# Metabat binning commands
binning_cmds = {}
binThreads = 14
bootstrapNum = 20
bootstrapSpecific = 10
minBinSize = 100000

for i, sample in enumerate(sample_names):
    cmd = (prgs["metabat"] + " -i {:} -o {:}/bin -a {:}/cov_var_table.txt "
           "--numThreads {:} --saveCls --unbinned -v -B {:} --pB {:} -s {:}")
    cmd = cmd.format(args.assembly_files[i], sample, sample, binThreads,
                     bootstrapNum, bootstrapSpecific, minBinSize)
    binning_cmds[sample] = cmd

#
# Running commands or adding them to queue
#

if(args.queue is not None):
    scripts_dir = args.tmp_dir + "/scripts"
    os.makedirs(scripts_dir, exist_ok=True)
    logs_dir = args.tmp_dir + "/logs"

    jobs = JobCluster(V=True, d=args.tmp_dir, outdir=logs_dir, errdir=logs_dir,
                      scripts=scripts_dir, job_interval=1,
                      max_procs_use=args.hpc_size, enable_balancing=True,
                      dependencies_file=args.config_prg,
                      conf_file=args.config_queue)

    indexing_jobs = {}

    for sample, cmd in index_cmds.items:
        job_id = sample + "_indexing"
        indexing_jobs[sample] = job_id
        cmd = shlex.quote(cmd)
        # TODO: Add appropriate resopurce demands for job
        jobs.addJob(jobid=job_id, cmd=cmd, nodes=1, ppn=2, mem="8gb",
                    walltime=43200)

    mapping_jobs = {}

    for read_file in read_files:
        for sample, cmd in map_cmds[read_file.filename].items:
            job_id = read_file.filename + "-" + sample + "_map"
            tmp = mapping_jobs.get(sample, [])
            tmp.append(job_id)
            mapping_jobs[sample] = tmp
            cmd = shlex.quote(cmd)
            # TODO: Add appropriate resopurce demands for job
            jobs.addJob(jobid=job_id, cmd=cmd, nodes=1, ppn=2, mem="8gb",
                        walltime=43200, depend=[indexing_jobs[sample]])

    joincovstat_jobs = {}

    for sample in sample_names:
        job_id = sample + "_joinCovtables"
        joincovstat_jobs[sample] = job_id
        # TODO: What are the paths to the covstat files? (Per sample)
        covstat_files = []
        # TODO: Where should output files be stored?
        out_path = ""
        cmd = (prgs["python3"] + " " + prgs["joinCovtables"]
               + " --output " + out_path + " "
               + covstat_files)
        cmd = shlex.quote(cmd)
        # TODO: Add appropriate resopurce demands for job
        jobs.addJob(jobid=job_id, cmd=cmd, nodes=1, ppn=2, mem="8gb",
                    walltime=43200, depend=mapping_jobs[sample])

    for sample, cmd in binning_cmds.items:
        job_id = sample + "_metabat"
        # TODO: Add appropriate resopurce demands for job
        jobs.addJob(jobid=job_id, cmd=cmd, nodes=1, ppn=2, mem="8gb",
                    walltime=43200, depend=[joincovstat_jobs[sample]])

    max_run_time = 2592000  # One month
    jobs.run(job_wait=True, max_wait=max_run_time)

else:
    # Executing indexing commands
    for cmd in index_cmds.values():
        cmd = shlex.quote(cmd)
        try:
            subprocess.run(cmd, shell=True, check=True)
        except subprocess.CalledProcessError as e:
            eprint("ERROR executing indexing command:\n " + cmd)
            eprint(e)
            quit(1)

quit(0)
