# README #

### Installation ###

#### Clone the repository ####
The --recursive flag is needed when you clone this repository.

~~~
git clone --recursive https://bitbucket.org/patrickmunk/mgbinner.git
~~~

### Updating ###
To update the repository a pull command and a update command is needed.
The following commands assume that the current working directory is the repository.

~~~
git pull
git submodule update --init --recursive
~~~