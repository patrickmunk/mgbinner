#! /usr/bin/python3

import argparse
import os
import os.path
import pandas as pd
import numpy as np


class CoverageTable():
    """ Module for parsing covstat files from from BBMap.
    """

    @staticmethod
    def covstat2CovVar(filename):
        """Function for for loading each individual coverage table
        Input: file name (string) of a tab-separated BBmap coverage table
        (scaffolds x variables) Returns: A 2-column table with just coverage
        and variance for each scaffold"""
        try:
            data = pd.read_csv(filename, sep="\t")
            data2 = data[['Avg_fold']]
            # Convert standard deviation to variance
            data2 = data2.assign(Var=np.array(data[['Std_Dev']])**2)
        # TODO: Can we make a more specific Exception?
        except Exception as e:
            print(e)
        return data2

    @classmethod
    def metaBatCoVarTable(cls, covfiles):
        """Function joining a number of coverage tables to a single cov/var
        table for all samples Input: A list of BBmap coverage files
        Returns: A table with name, coverage, variance for all scaffolds in all
        samples This format is used by MetaBat"""
        try:
            # For the 1st coverage table, additional information on scaf length
            # is taken
            data = pd.read_csv(covfiles[0], sep="\t")
            data2 = data[['#ID', 'Length', 'Avg_fold']]
            dataNew = data2.assign(Var=np.array(data[['Std_Dev']])**2)

            # Find the sample name and make column names unique
            samplename = os.path.basename(covfiles[0])
            samplename = samplename.replace("_covstats.txt", "")
            dataNew.columns = ["scafname", "Length", samplename + "_COV",
                               samplename + "_VAR"]
            # For the remaining coverage tables,
            # just coverage and var is taken, using covStat2CovVar
            covfilesRemain = covfiles[1:]
            for covfile in covfilesRemain:

                data = cls.covstat2CovVar(covfile)
                samplename = os.path.basename(covfile)
                samplename = samplename.replace("_covstats.txt", "")
                data.columns = [samplename + "_COV", samplename + "_VAR"]
                dataNew = pd.concat([dataNew, data], axis=1)
            # Calculate the average coverage for the 3rd column
            covdf = dataNew.ix[:, range(2, dataNew.shape[1], 2)]
            avgCov = np.mean(covdf, axis=1)
            dataNew.insert(loc=2, column='mean_COV', value=avgCov)
            # Make a new dataframe from the old and with avg_cov as the 4th
            # column
            return dataNew
        # TODO: Can we make a more specific Exception?
        except Exception as e:
            print(e)


if __name__ == '__main__':

    #
    # Handling arguments
    #
    parser = argparse.ArgumentParser(description="Takes a number of BBmap\
        covstat files and produces a coverage / variance table for MetaBat\
        binning.")

    # Input / Output options
    parser.add_argument("covstat_files",
                        help="BBMap 'covstat' files.",
                        metavar='TXT',
                        default=None)
    parser.add_argument("-o", "--output",
                        help="Path to output file.",
                        default=None,
                        metavar='OUT_DIR')

    args = parser.parse_args()

    # Fake data
    # args.covstat_files = ["testData/sample1_covstats.txt", "testData/sample2_covstats.txt", "testData/sample3_covstats.txt"]

    # Create a final coverage table with scaffold rows and name, length,
    # avg_cov, sample1cov, sample1var, sampleNcov, sampleNvar
    metabat_tab = CoverageTable.metaBatCoVarTable(args.covstat_files)

    # Export the metabat-ready coverage table
    metabat_tab.to_csv("metabat_coVar.txt", sep="\t", float_format='%11.5f',
                       index=False)
